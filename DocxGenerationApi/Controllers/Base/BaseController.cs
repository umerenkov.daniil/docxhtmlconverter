﻿using System;
using System.Linq;
using DocxGenerationApi.Models.Response;
using DocxGenerationApi.Models.Response.Enums;
using Domain.Exceptions;
using Microsoft.AspNetCore.Mvc;

namespace DocxGenerationApi.Controllers.Base
{
    public abstract class BaseController : Controller
    {
        public virtual ApiResponse<TData> Ack<TData>(TData data)
        {
            return new ApiResponse<TData>(data);
        }

        public virtual ApiResponse<TData> Nac<TData>(Exception originalException)
        {
            var error = new Error();
            var code = ApiResponseCode.UNDEFINED_ERROR;

            if (originalException is AppException appException)
            {
                error.Errors = appException.Errors.ToArray();
                code = ApiResponseCode.APP_ERROR;
            }
            else error.Errors = new[] {originalException.Message};

            return new ApiResponse<TData>(default, code, error);
        }
    }
}
