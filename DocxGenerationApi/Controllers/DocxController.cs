﻿using System;
using System.IO;
using System.Threading.Tasks;
using DocxGenerationApi.Controllers.Base;
using DocxGenerationApi.Services.Converters;
using Domain.Converters.Enums;
using Domain.Exceptions;
using Domain.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace DocxGenerationApi.Controllers
{
    [Route("api/[controller]")]
    public class DocxController : BaseController
    {
        private readonly DocxConverterService _docxConverterService;
        private readonly ILogger<DocxController> _logger;

        public DocxController(
            DocxConverterService docxConverterService,
            ILogger<DocxController> logger)
        {
            _docxConverterService = docxConverterService;
            _logger = logger;
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> DocxToHtml(IFormFile file)
        {
            try
            {
                if (file == null) throw new AppException("File is null");

                var htmlStream = await _docxConverterService.DocxToHtml(file);
                return new FileStreamResult(htmlStream, DictionaryHelper.MediaTypes.Names[FileType.ZIP]);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "DocxToHtml failure");
                return BadRequest();
            }
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> HtmlToDocx(IFormFile file)
        {
            try
            {
                if (file == null) throw new AppException("File is null");

                var docxStream = await _docxConverterService.HtmlToDocx(file);
                return new FileStreamResult(docxStream, DictionaryHelper.MediaTypes.Names[FileType.DOCX]);

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "HtmlToDocx failure");
                return BadRequest();
            }
        }
    }
}