﻿using System.IO;
using DocxGenerationApi.Services;
using DocxGenerationApi.Services.Converters;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using WordInterop;

namespace DocxGenerationApi
{
    public static class Initial
    {
        public static IServiceCollection AddCustomServices(this IServiceCollection collection)
        {
            collection.AddSingleton<TemporaryFilesService>();
            collection.AddScoped<DocxConverterService>();
            collection.AddScoped<ZipService>();

            return collection;
        }

        public static void Run(IApplicationBuilder applicationBuilder) {}
    }
}
