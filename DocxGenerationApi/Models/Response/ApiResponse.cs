﻿using DocxGenerationApi.Models.Response.Enums;

namespace DocxGenerationApi.Models.Response
{
    public class ApiResponse<TData>
    {
        public TData Data { get; set; }
        public ApiResponseCode Code { get; set; }
        public Error Error { get; set; }

        public ApiResponse(TData data)
        {
            Data = data;
            Code = ApiResponseCode.OK;
            Error = null;
        }

        public ApiResponse(TData data, ApiResponseCode code, Error error)
        {
            Data = data;
            Code = code;
            Error = error;
        }
    }
}
