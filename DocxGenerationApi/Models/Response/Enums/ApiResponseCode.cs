﻿namespace DocxGenerationApi.Models.Response.Enums
{
    public enum ApiResponseCode
    {
        NONE = 0,
        OK = 1,
        APP_ERROR = 2,
        UNDEFINED_ERROR = 3
    }
}
