﻿namespace DocxGenerationApi.Models.Response
{
    public class Error
    {
        public string[] Errors { get; set; }
    }
}
