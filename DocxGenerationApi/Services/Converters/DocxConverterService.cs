﻿using System.IO;
using System.Threading.Tasks;
using Domain.Converters.Enums;
using Microsoft.AspNetCore.Http;
using WordInterop;

namespace DocxGenerationApi.Services.Converters
{
    public class DocxConverterService
    {
        private readonly TemporaryFilesService _temporaryFilesService;
        private readonly ZipService _zipService;

        public DocxConverterService(
            TemporaryFilesService temporaryFilesService,
            ZipService zipService)
        {
            _temporaryFilesService = temporaryFilesService;
            _zipService = zipService;
        }

        public async Task<MemoryStream> DocxToHtml(IFormFile file)
        {
            var filePathAsync = Task.Run(() => _temporaryFilesService.CreateTmpFileAsync(file));
            FileInfo savedFile;

            using (var wordAccessor = new WordAccessor())
            {
                await Task.WhenAll(filePathAsync, wordAccessor.InstanceCreated);

                var document = wordAccessor.OpenFile(filePathAsync.Result);
                wordAccessor.SetEncoding(document);

                savedFile = _temporaryFilesService.CreateEmptyTypeFile(FileType.HTML);

                wordAccessor.SaveHtml(savedFile, document);
                wordAccessor.Close(document);
            }

            var zipFiles = _zipService.GetZipFiles(savedFile);
            var zipMemory = _zipService.Zip(zipFiles);

            _temporaryFilesService.DeleteTempFile(filePathAsync.Result);
            _temporaryFilesService.DeleteTempHtml(savedFile);

            return zipMemory;
        }

        public async Task<FileStream> HtmlToDocx(IFormFile file)
        {
            var fileBaseAsync = Task.Run(() => _zipService.UnZip(file));
            FileInfo savedFile;

            using (var wordAccessor = new WordAccessor())
            {
                await Task.WhenAll(fileBaseAsync, wordAccessor.InstanceCreated);

                var unzipHtmlBase = _temporaryFilesService.SearchForFileInTmp(fileBaseAsync.Result);

                var document = wordAccessor.OpenFile(unzipHtmlBase);
                wordAccessor.SetEncoding(document);

                savedFile = _temporaryFilesService.CreateEmptyTypeFile(FileType.DOCX);

                wordAccessor.SaveDocx(savedFile, document);
                wordAccessor.Close(document);
            }

            return File.OpenRead(savedFile.FullName);
        }
    }
}