﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Domain.Converters.Enums;
using Domain.Exceptions;
using Domain.Helpers;
using Microsoft.AspNetCore.Http;

namespace DocxGenerationApi.Services
{
    public class TemporaryFilesService
    {
        public string TempDirectoryPath => Path.GetTempPath();

        public TemporaryFilesService() {}

        public async Task<FileInfo> CreateTmpFileAsync(IFormFile file)
        {
            var tmpFile = Path.GetTempFileName();

            using (var inputFileStream = file.OpenReadStream())
            using (var tmpFileStream = File.OpenWrite(tmpFile))
                await inputFileStream.CopyToAsync(tmpFileStream);

            return new FileInfo(tmpFile);
        }

        public FileInfo CreateEmptyTypeFile(FileType type)
        {
            string filePath;

            do
            {
                var fileName = Guid.NewGuid() + GetExtension(type);
                filePath = Path.GetFullPath(Path.Combine(TempDirectoryPath, fileName));
            } while (File.Exists(filePath));

            return new FileInfo(filePath);
        }

        public IEnumerable<DirectoryInfo> GetHtmlAdditionalDirectories(FileInfo file)
        {
            var possibleMatchName = Path.GetFileNameWithoutExtension(file.Name);

            if (file.Directory == null) throw new AppException("Directory not found");
            return Directory.GetDirectories(file.Directory.FullName)
                .Select(x => new DirectoryInfo(x))
                .Where(x => x.Name.StartsWith(possibleMatchName))
                .ToArray();
        }

        public void DeleteTempFile(FileInfo file)
        {
            File.Delete(file.FullName);
        }

        public void DeleteTempHtml(FileInfo file)
        {
            var directories = GetHtmlAdditionalDirectories(file);

            Parallel.ForEach(directories, directory => Directory.Delete(directory.FullName, true));
            File.Delete(file.FullName);
        }

        public string CreateDirectory(string directory)
        {
            var path = Path.GetFullPath(Path.Join(TempDirectoryPath, directory));
            if (!Directory.Exists(path)) Directory.CreateDirectory(path);

            return path;
        }

        public FileInfo SearchForFileInTmp(string fileName)
        {
            var matches = Directory.GetFiles(TempDirectoryPath)
                .Select(x => new FileInfo(x))
                .Where(x => x.Name.StartsWith(fileName))
                .ToList();

            if (matches.Count != 1) throw new AppException("File not found");

            return matches.Single();
        }

        private static string GetExtension(FileType type)
        {
            var valueExists = DictionaryHelper.FileTypeExtensionName.Names.TryGetValue(type, out var value);
            if (valueExists) return value;
            throw new AppException("File type not found");
        }
    }
}
