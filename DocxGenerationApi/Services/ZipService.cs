﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Converters.Enums;
using Domain.Exceptions;
using Domain.Helpers;
using Microsoft.AspNetCore.Http;

namespace DocxGenerationApi.Services
{
    public class ZipService
    {
        private readonly TemporaryFilesService _temporaryFilesService;

        public ZipService(TemporaryFilesService temporaryFilesService)
        {
            _temporaryFilesService = temporaryFilesService;
        }

        public MemoryStream Zip(Dictionary<string, string> entities)
        {
            // async test

            var memoryStream = new MemoryStream();
            using (var archive = new ZipArchive(memoryStream, ZipArchiveMode.Create, true, Encoding.UTF8))
            foreach (var entity in entities)
            {
                var zipEntry = archive.CreateEntry(entity.Key);

                using (var zipStream = zipEntry.Open())
                using (var fileStream = File.OpenRead(entity.Value))
                fileStream.CopyTo(zipStream);
            }

            memoryStream.Seek(0, SeekOrigin.Begin);

            return memoryStream;
        }

        public string UnZip(IFormFile file)
        {
            string name;

            using (var mainStream = file.OpenReadStream())
            using (var archive = new ZipArchive(mainStream, ZipArchiveMode.Read, true, Encoding.UTF8))
            {
                var notNullEntries = archive.Entries.Where(x => !string.IsNullOrEmpty(x.Name)).ToArray();

                foreach (var entry in notNullEntries)
                {
                    var split = entry.FullName.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                    string path;

                    if (split.Length > 1)
                    {
                        var directory = string.Join("//", split.SkipLast(1));
                        path = _temporaryFilesService.CreateDirectory(directory);
                    }
                    else path = _temporaryFilesService.TempDirectoryPath;

                    path = Path.GetFullPath(Path.Combine(path, entry.Name));

                    using (var fileStream = File.Create(path))
                    using (var entityStream = entry.Open())
                        entityStream.CopyTo(fileStream);
                }

                var htmlEnding = DictionaryHelper.FileTypeExtensionName.Names[FileType.HTML];
                name = Path.GetFileNameWithoutExtension(notNullEntries.FirstOrDefault(x => x.FullName.EndsWith(htmlEnding))?.Name);
            }

            return name;
        }

        public Dictionary<string, string> GetZipFiles(FileInfo file)
        {
            var data = new Dictionary<string, string> { { file.Name, file.FullName } };
            var directories = _temporaryFilesService.GetHtmlAdditionalDirectories(file);

            foreach (var directory in directories) GetDirectoryFiles(data, directory, $"{directory.Name}/");

            return data;
        }

        private void GetDirectoryFiles(Dictionary<string, string> data, DirectoryInfo source, string prefix)
        {
            var entities = source.GetFileSystemInfos();

            foreach (var entity in entities)
            {
                switch (entity)
                {
                    case FileInfo file:
                        data.Add(prefix + file.Name, file.FullName);
                        break;
                    case DirectoryInfo directory:
                        GetDirectoryFiles(data, directory, $"{prefix}{entity.Name}/");
                        break;
                }
            }
        }
    }
}
