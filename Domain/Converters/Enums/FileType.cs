﻿namespace Domain.Converters.Enums
{
    public enum FileType
    {
        NONE = 0,
        DOCX = 1,
        HTML = 2,
        ZIP = 3
    }
}
