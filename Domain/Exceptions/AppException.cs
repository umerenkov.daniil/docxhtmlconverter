﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace Domain.Exceptions
{
    public class AppException : Exception
    {
        public List<string> Errors { get; set; }

        public AppException()
        {
            Errors = null;
        }

        protected AppException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public AppException(params string[] messages) : base(string.Join(' ', messages))
        {
            Errors = new List<string>();
            Errors.AddRange(messages);
        }

        public AppException(string message, Exception innerException) : base(message, innerException)
        {
            Errors = new List<string> { message };
        }

        public AppException(IEnumerable<string> messages) : base(string.Join(' ', messages))
        {
            Errors = messages.ToList();
        }
    }
}
