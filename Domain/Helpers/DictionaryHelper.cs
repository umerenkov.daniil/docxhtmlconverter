﻿using System.Collections.Generic;
using System.Reflection;
using Domain.Converters.Enums;
using Domain.Interop;
using Domain.Interop.Enums;

namespace Domain.Helpers
{
    public static class DictionaryHelper
    {
        public static class FileTypeExtensionName
        {
            public static Dictionary<FileType, string> Names = new Dictionary<FileType, string>
            {
                {FileType.DOCX, ".docx"},
                {FileType.HTML, ".htm"}
            };
        }

        public static class MediaTypes
        {
            public static Dictionary<FileType, string> Names = new Dictionary<FileType, string>
            {
                {FileType.ZIP, "application/zip"},
                {FileType.DOCX, "application/vnd.openxmlformats-officedocument.wordprocessingml.document"}
            };
        }

        public static class InteropBindings
        {
            public static Dictionary<InteropAction, Invocation> Actions = new Dictionary<InteropAction, Invocation>
            {
                {InteropAction.GET_DOCUMENTS, new Invocation("Documents", BindingFlags.GetProperty)},
                {InteropAction.OPEN_DOCUMENT, new Invocation("Open", BindingFlags.InvokeMethod)},
                {InteropAction.GET_WEB_OPTIONS, new Invocation("WebOptions", BindingFlags.GetProperty)},
                {InteropAction.SET_ENCODING, new Invocation("Encoding", BindingFlags.SetProperty)},
                {InteropAction.SAVE_HTML, new Invocation("SaveAs2", BindingFlags.InvokeMethod)},
                {InteropAction.SAVE_DOCX, new Invocation("SaveAs", BindingFlags.InvokeMethod)},
                {InteropAction.CLOSE, new Invocation("Close", BindingFlags.InvokeMethod)},
                {InteropAction.QUIT, new Invocation("Quit", BindingFlags.InvokeMethod)}
            };
        }
    }
}
