﻿namespace Domain.Interop.Enums
{
    public enum InteropAction
    {
        NONE = 0,
        GET_DOCUMENTS = 1,
        OPEN_DOCUMENT = 2,
        GET_WEB_OPTIONS = 3,
        SET_ENCODING = 4,
        SAVE_HTML = 5,
        SAVE_DOCX = 6,
        CLOSE = 7,
        QUIT = 8
    }
}
