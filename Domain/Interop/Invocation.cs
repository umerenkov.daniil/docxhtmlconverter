﻿using System.Reflection;

namespace Domain.Interop
{
    public class Invocation
    {
        public string PropertyName { get; set; }
        public BindingFlags BindingFlags { get; set; }
        public Binder Binder { get; set; }

        public Invocation(string propertyName, BindingFlags bindingFlags)
        {
            PropertyName = propertyName;
            BindingFlags = bindingFlags;
            Binder = null;
        }

        public object Trigger(object instance, params object[] values)
        {
            return instance
                .GetType()
                .InvokeMember(
                    PropertyName,
                    BindingFlags,
                    Binder,
                    instance,
                    values);
        }
    }
}
