﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Domain.Helpers;
using Domain.Interop;
using Domain.Interop.Enums;

namespace WordInterop
{
    public class WordAccessor : IDisposable
    {
        private const string WORD_PROGRAM_ID = "Word.Application";

        private readonly Dictionary<InteropAction, Invocation> _actions;

        public object Instance;
        public Task InstanceCreated;

        public WordAccessor()
        {
            _actions = DictionaryHelper.InteropBindings.Actions;
            CreateInstance();
        }

        public object OpenFile(FileInfo file)
        {
            InstanceCreated.Wait();

            var documents = _actions[InteropAction.GET_DOCUMENTS].Trigger(Instance);
            return _actions[InteropAction.OPEN_DOCUMENT].Trigger(documents, file.FullName);
        }

        public void SetEncoding(object document)
        {
            var webOptions = _actions[InteropAction.GET_WEB_OPTIONS].Trigger(document);
            _actions[InteropAction.SET_ENCODING].Trigger(webOptions, 0x0000FDE9);
        }

        public void SaveHtml(FileInfo file, object document)
        {
            _actions[InteropAction.SAVE_HTML].Trigger(document, file.FullName, 8);
        }

        public void SaveDocx(FileInfo file, object document)
        {
            _actions[InteropAction.SAVE_DOCX].Trigger(document, file.FullName, 16);
        }

        public void Close(object document)
        {
            _actions[InteropAction.CLOSE].Trigger(document);
        }

        private void CreateInstance()
        {
            InstanceCreated = Task.Run(() =>
            {
                var type = Type.GetTypeFromProgID(WORD_PROGRAM_ID);
                Instance = Activator.CreateInstance(type);
            });
        }

        public void Dispose()
        {
            InstanceCreated.Wait();
            _actions[InteropAction.QUIT].Trigger(Instance);

            InstanceCreated?.Dispose();
        }
    }
}
